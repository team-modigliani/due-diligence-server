'use strict'

import { join } from 'path';
import { ApolloServer } from 'apollo-server-express';
import { makeAugmentedSchema } from 'neo4j-graphql-js';
import neo4j from 'neo4j-driver';

import { typeDefs } from './schema/graphql-schema';
import { resolvers } from './schema/resolvers';

import express from 'express';
import cors from 'cors';

import { CompaniesHouse } from './integrations/CompaniesHouse';
import { OpenCorporates } from './integrations/OpenCorporates';
import { WebhoseIo } from './integrations/WebhoseIo';
import { Snowflake } from './integrations/Snowflake';
import User from './integrations/User';
import Connection from './helpers/db';
import TokenMiddleware from './helpers/tokenMiddleware';
import tokens from './helpers/tokens';

const compression = require('compression');

require('dotenv').config();

const DATABASE_URI = process.env.DATABASE_URI || '';
const DATABASE_USER = process.env.DATABASE_USER || 'neo4j';
const DATABASE_PASSWORD = process.env.DATABASE_PASSWORD || 'password';
const EXPRESS_PORT = process.env.PORT || 4000;
const WEBHOSE_API_KEY = process.env.WEBHOSE_API_KEY || '';

const driver = neo4j.driver(
  DATABASE_URI,
  neo4j.auth.basic(DATABASE_USER, DATABASE_PASSWORD)
);

const schema = makeAugmentedSchema({
  typeDefs,
  resolvers,
  config: {
    query: true, // default
    mutation: true,
    auth: {
      hasScope: true,
      hasRole: true,
      isAuthenticated: true
    }
  },
  inheritResolversFromInterfaces: false,
});

const app = express();
app.use(cors());
app.use(compression());
//app.use(TokenMiddleware.validateTokenMiddleware);

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({
  schema,
  resolvers,
  dataSources: () => ({
    //companiesHouse: new CompaniesHouse(),
    //openCorporates: new OpenCorporates(),
    snowflake: new Snowflake(),
    user: new User(),
  }),
  context: ({ req, res }) => {
    const user = tokens.validateAccessToken(req.headers.authorization);
    return { user, driver, req, res }
  }
});

server.applyMiddleware({ app, cors: false });
Connection.connect();

// The `listen` method launches a web server.
app.listen({ port: EXPRESS_PORT }, () =>
  console.log(`🚀  Server ready at ${server.graphqlPath}:${EXPRESS_PORT}`)
);

