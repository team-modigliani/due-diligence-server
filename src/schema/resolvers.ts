import { neo4jgraphql } from 'neo4j-graphql-js';

export const resolvers = {
    Query: {
        Client:(root, args, context, resolveInfo) => {
            //TODO: Need to add logic here!!!
            if(context.user.role !== 'ADMIN') {
                resolveInfo.fieldNodes.forEach(element => {
                    if(element.arguments.length) {
                        element.arguments.forEach(item => {
                            if (item.name.value !== 'clientId') {
                                throw new Error('you are not authorized to execute this query');
                            }
                        });
                    } else {
                        throw new Error('you are not authorized to execute this query');
                    }
                });
            }

            return neo4jgraphql(root, args, context, resolveInfo);
        },
        Report:(root, args, context, resolveInfo) => {
            console.log("Unless admin, must have ID!!!");
            return neo4jgraphql(root, args, context, resolveInfo);
        },
        Investigation:(root, args, context, resolveInfo) => {
            console.log("Unless admin, must have ID!!!");
            return neo4jgraphql(root, args, context, resolveInfo);
        },
        Intelligence:(root, args, context, resolveInfo) => {
            console.log("Unless admin, must have ID!!!");
            return neo4jgraphql(root, args, context, resolveInfo);
        }

    },
    Mutation: {
        CreateUUID: async (_source, { count }, { dataSources }) => dataSources.snowflake.CreateUUID(count),
        CreateLogin: async (_source, { login, username, password, role, group }, { dataSources }) => dataSources.user.CreateLogin(login, username, password, role, group),
        Login: async (_source, { login, password }, { dataSources }) => dataSources.user.Login(login, password)
    },
};
