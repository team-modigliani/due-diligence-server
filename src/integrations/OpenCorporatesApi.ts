import { RESTDataSource, RequestOptions } from 'apollo-datasource-rest';

module.exports = class OpenCorporates extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://api.opencorporates.com';
  }

  willSendRequest(request: RequestOptions) {
    request.headers.set('content-type', 'application/json;charset=utf-8');
  }

  async getCompany(id: Number) {
    const data = await this.get(`/company/${id}`);
    return this.companyReducer(data);
  }

  async getCompanies(search: String) {
      const data = await this.get('/companies/search', {
          q: search
      });

      const companies = data.results.companies.map(item => {
          return this.companyReducer(item);
      })

      console.log(companies);
      return companies;
  }

  companyReducer(data) {
    return {
        number: data.company.company_number || '',
        name: data.company.name || '',
        status: data.company.current_status || '',
        type: data.company.company_type || '',
        jurisdiction: data.company.jurisdiction_code || '',
        incorporation_date: data.company.incorporation_date || '',
        dissolution_date: data.company.dissolution_date || ''
    }
  }
}