
import webhoseio from 'webhoseio';
import { RESTDataSource } from 'apollo-datasource-rest';

let WEBHOSE_API_KEY;

export class WebhoseIo extends RESTDataSource {
    constructor(API_KEY) {
        super();
        WEBHOSE_API_KEY = API_KEY;
    }

    async getIntelligence(searchTerm) {
        let date =  new Date();
        let timestamp = date.setDate(date.getDate() - 30);

        const client = webhoseio.config({ token: WEBHOSE_API_KEY });
        const query_params = {
            q: searchTerm,
            ts: timestamp,
            sort: "crawled"
        }

        const data = await client.query('filterWebContent', query_params);
        const intelligence = data.posts.map(item => {
            return this.intelligenceReducer(item);
        });

        return intelligence;
    }

    intelligenceReducer(data) {
        return {
            name: data.thread.title_full || '',
            source: data.thread.url || '',
            type: data.thread.site_type || ''
        }
      }
}
