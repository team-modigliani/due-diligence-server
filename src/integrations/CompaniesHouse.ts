import { RESTDataSource } from 'apollo-datasource-rest';
import dotenv  from 'dotenv';

dotenv.config();
const COMPANIES_HOUSE_API_KEY = process.env.COMPANIES_HOUSE_API_KEY || '';

export class CompaniesHouse extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://api.companieshouse.gov.uk';
  }

  willSendRequest(request) {
      request.headers.set('Authorization', COMPANIES_HOUSE_API_KEY)
  }

  async getCompany(id) {
    const data = await this.get(`/company/${id}`);
    return this.companyReducer(data);
  }

  async getCompanies(search) {
      const data = await this.get('/search/companies', {
          q: search
      });

      const companies = data.items.map(item => {
          return this.companyReducer(item);
      })

      return companies;
  }

  companyReducer(data) {
    return {
      name: data.company_name,
      type: data.type || data.company_type,
      status: data.company_status || data.status,
      creation_date: data.date_of_creation,
      cessation_date: data.date_of_cessation
    }
  }
}
