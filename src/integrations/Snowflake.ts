import FlakeId from 'flake-idgen';
import Format from 'biguint-format';
import { RESTDataSource } from 'apollo-datasource-rest';

export class Snowflake extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = '';
    }

    async CreateUUID(count: Number) {
        const id = new FlakeId();
        let i:number = 0;
        let items:any[] = [];

        do {
            const value:any = Format(id.next(), 'hex', { prefix: '0x' });
            items.push(value);
            i = i + 1;
        } while (i < count);

        return {
            uuid: items
        }
    }
}
