import { RESTDataSource } from 'apollo-datasource-rest';

export class OpenCorporates extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://api.opencorporates.com';
  }

  willSendRequest(request) {
    request.headers.set('content-type', 'application/json;charset=utf-8');
  }

  async getCompany(id) {
    const data = await this.get(`/company/${id}`);
    return this.companyReducer(data);
  }

  async getCompanies(searchTerm: String) {
      const data = await this.get('/companies/search', {
          q: searchTerm
      });

      const companies = data.results.companies.map(item => {
          return this.companyReducer(item);
      })

      return companies;
  }

  companyReducer(data) {
    return {
        name: data.company.name || '',
        status: data.company.current_status || '',
        type: data.company.company_type || '',
        creation_date: data.company.incorporation_date || '',
        cessation_date: data.company.dissolution_date || ''
    }
  }
}