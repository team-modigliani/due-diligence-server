import { RESTDataSource } from 'apollo-datasource-rest';
import Hash from '../helpers/hash';
import Connection from '../helpers/db';
import Tokens from '../helpers/tokens';

export default class User extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = '';
  }

  async CreateLogin(login: string, username: String, password: String, role: String, group: String) {
    const client = await Connection.connect();
    const hash = new Hash();
    const document = {
      login,
      username,
      password: hash.generatePasswordHash(password),
      role,
      group
    }

    const result = await client.db().collection('userCollection').insertOne(document);
    return document;
  }

  async IsValidLogin(password: String, dbPassword: String) {
    const hash = new Hash();
    const data = dbPassword.split('#');
    const hashedPassword = hash.generatePasswordHash(password, data[0]);
    if(hashedPassword === dbPassword) {
      return true;
    }

    return false;
  }

  async Login(login: String, password: String) {
    const result = await User.GetUser(login);
   
    if(!result) {
      return null;
    }

    const passwordValid = await this.IsValidLogin(password, result.password);

    if (!passwordValid) {
      return null;
    }

    return Tokens.setToken(result);
  }

  static async GetUser(login: String) {
    const client = await Connection.connect();
    const result = await client.db().collection('userCollection').findOne({login: login});

    if(!result) {
      return null;
    }

    return {
      id: result._id,
      login: result.login,
      username: result.username,
      password: result.password,
      role: result.role,
      group: result.group ? result.group : ''
    }
  }
}