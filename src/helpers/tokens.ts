import { sign, verify } from 'jsonwebtoken';
import Scopes  from './scopes';

export default class Tokens {
  static setToken(userDetails:any) {

    const user = {
      id: userDetails.id,
      username: userDetails.username,
      role: userDetails.role,
      group: userDetails.group,
      scope: Scopes.getScope(userDetails.role) 
    };

    const accessToken = sign(
      user,
      "secretKey1",
      {
        expiresIn: '15m'
      }
    );
  

    return { accessToken, user };
  }

  static validateAccessToken(token) {
    try {
      return verify(token.replace("Bearer ", ""), "secretKey1");
    } catch {
      return null;
    }
  }
}