export default class Scopes {
  static getScope(role: string) {
    if (role === 'ADMIN') {
      return [
        'Node: Create',
        'Node: Read',
        'Client: Create',
        'Report: Create',
        'Section: Create',
        'Intelligence: Create',
        'Investigation: Create',
        'Mandate: Create',
        'Person: Create',
        'Document: Create',
        'Client: Read',
        'Report: Read',
        'Section: Read',
        'Intelligence: Read',
        'Investigation: Read',
        'Mandate: Read',
        'Person: Read',
        'Document: Read',
        'Person: Update',
        'Location: Create',
        'Location: Read',
        'Area: Create',
        'Area: Read',
        'Address: Create',
        'Address: Read',
        'Object: Create',
        'Object: Read',
        'Event: Read',
        'Event: Create',
        'Media: Create',
        'Media: Read',
        'Comment: Read',
        'Comment: Create'
      ];
    }

    if (role === 'USER') {
      return [
        'Client: Read',
        'Report: Read',
        'Section: Read',
        'Intelligence: Read',
        'Investigation: Read',
        'Mandate: Read',
        'Person: Read',
        'Document: Read',
        'Location: Read',
        'Area: Read',
        'Address: Read',
        'Object: Read',
        'Event: Read',
        'Media: Read',
        'Node: Read',
        'Comment: Read'
      ];      
    }
  }
}