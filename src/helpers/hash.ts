import crypto from 'crypto';
import dotenv  from 'dotenv';

dotenv.config();
const pepper = process.env.PEPPER || '';

export default class Hash {
  generatePasswordHash(value: String, salt?: any) {
    salt = salt || crypto.randomBytes(64).toString('hex');
    const hash = crypto.pbkdf2Sync(pepper + value, salt, 1000000, 64, 'sha512').toString('hex');
    return `${salt}#${hash}`;
  }
  
}
