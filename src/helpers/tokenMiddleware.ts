import Tokens from './tokens';
import User from '../integrations/User';

export default class TokenMiddleware {
  static async validateTokenMiddleware(req, res, next) {
    //const refreshToken = req.headers["x-refresh-token"];
    const accessToken = req.headers["authorization"];
    
    if (!accessToken) {
      return next();
    }

    const decodedAccessToken = Tokens.validateAccessToken(accessToken);

    if (decodedAccessToken && decodedAccessToken.user) {
      req.user = decodedAccessToken.user;
      //return next();
    //}

    //const decodedRefreshToken = Tokens.validateRefreshToken(refreshToken);

    //if (decodedRefreshToken && decodedRefreshToken.user) {

      // valid refresh token
      //const user = await User.GetUser(decodedRefreshToken.user.username);

      // valid user and user token not invalidated
      //if (!user || user.tokenCount !== decodedRefreshToken.user.count) {
      //  return next();
     // }

      //req.user = decodedRefreshToken.user;

      const userTokens = Tokens.setToken(req.user);
      res.set({
        "Access-Control-Expose-Headers": "authorization",
        "authorization": `Bearer ${userTokens.accessToken}`
      });

      return next();
    }

    next();   
  }
}
