import { MongoClient } from 'mongodb';

let db:any;
const url:String = 'mongodb+srv://georges:3Bpao4PhlNq1LJZMznXicbXo@suerat.dug2v.gcp.mongodb.net/seurat?retryWrites=true&w=majority';
const options:Object = {
  bufferMaxEntries: 0,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

export default class Connection {

  static async connect() {
    if(!db) {
      console.log('connecting');
      db = await MongoClient.connect(url, options);
    }
    
    return db;
  }
}

/*
const MongoClient = require('mongodb').MongoClient

class Connection {
    static connectToMongo() {
        if ( this.db ) return Promise.resolve(this.db)
        return MongoClient.connect(this.url, this.options)
            .then(db => this.db = db)
    }

    // or in the new async world
    static async connectToMongo() {
        if (this.db) return this.db
        this.db = await MongoClient.connect(this.url, this.options)
        return this.db
    }
}

Connection.db = null
Connection.url = 'mongodb://127.0.0.1:27017/test_db'
Connection.options = {
    bufferMaxEntries:   0,
    reconnectTries:     5000,
    useNewUrlParser:    true,
    useUnifiedTopology: true,
}

module.exports = { Connection }
*/