// APOC trigger for giveing every new node a timestamp. (Note: trigger is not currently supported with neo4j aura)
CALL apoc.trigger.add('timestampNewNodes', "UNWIND {createdNodes} AS n SET n.node_created = timestamp()", {phase:'before'});
CALL apoc.trigger.add('timestampUpdatedNodes', "UNWIND apoc.trigger.nodesByLabel({assignedNodeProperties}, null) AS n SET n.node_updated = timestamp()", {phase:'before'});

//Setup an index for searching People, Entities and Intelligence by the name property of the node
CALL db.index.fulltext.createNodeIndex("nameSearch", ["Person", "Entity", "Intelligence"], ["name"])